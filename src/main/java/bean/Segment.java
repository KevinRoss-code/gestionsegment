package bean;

import java.util.ArrayList;
import java.util.List;

public class Segment {
	private int extr1;
	private int extr2;
	 
	
	public int getExtr1() {
		return extr1;
	}
	public void setExtr1(int extr1) {
		this.extr1 = extr1;
	}
	public int getExtr2() {
		//Authentification
		return extr2;
	}
	public void setExtr2(int extr2) {
		this.extr2 = extr2;
	}
	


	public Segment(int a,int b){
		extr1=a;extr2=b;ordonne();
	}
	public void ordonne(){
		if(extr1>extr2){
			int z=extr1;
			extr1=extr2;
			extr2=z;
		}
	}
	public int getLongueur(){
		return(extr2-extr1);
	}
	public boolean appartient(int x){
		if((x>extr1)&&(x<extr2))
			return true;
		else return false;
	}
	public String toString(){
		return ("segment["+extr1+","+extr2+"]");
	}
}
