package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import bean.Segment;

class SegmentTest {

	@Test
	void testLongeur() {
		Segment monSegment= new Segment(2,6);
		int longeur=monSegment.getLongueur();
		assertEquals(4, longeur);
	}
	
	@Test
	void testAppartient() {
		Segment monSegment= new Segment(2,6);
		boolean appartient=monSegment.appartient(4);
		assertEquals(true, appartient);
	}

}
